﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LAPMTech
{
    public class StartGame : MonoBehaviour
    {
        [SerializeField]
        private string scenename;
        // Start is called before the first frame update
        void Start()
        {

        }

        public void GameScene()
        {
            SceneManager.LoadScene(scenename);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
