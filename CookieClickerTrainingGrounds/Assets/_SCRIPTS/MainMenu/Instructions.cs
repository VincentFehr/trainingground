﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    public class Instructions : MonoBehaviour
    {
        [SerializeField]
        private GameObject instructionsWindow;

        // Start is called before the first frame update
        void Start()
        {

        }

        public void instructions()
        {
            instructionsWindow.gameObject.SetActive(true);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
