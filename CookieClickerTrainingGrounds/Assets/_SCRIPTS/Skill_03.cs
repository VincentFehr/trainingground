﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
    public class Skill_03 : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI cost3;

        [SerializeField]
        private SkillObjects skillobject3;

        [SerializeField]
        private Clicker clicker;

        [SerializeField]
        private Button button;

        private bool knowledgeBoost;

        public bool KnowledgeBoost
        {
            get
            {
                return knowledgeBoost;
            }
            set
            {
                knowledgeBoost = value;
            }
          
        }

        private float skill3AppliedEffect;

        // Start is called before the first frame update

        void Start()
        {
            button.onClick.AddListener(SkillActivate);
            skillobject3.expPerSkillPoint = 1.25f;
            skillobject3.skillPointsInvested = 0;
            skillobject3.expCost = 3000;
        }

        void SkillActivate()
        {
            if (clicker.ExperiencePoints >= skillobject3.expCost)
            {
                clicker.ExperiencePoints -= skillobject3.expCost;

                skillobject3.skillPointsInvested += 1;

                clicker.SomethingWasBought = true;

                button.interactable = false;

                skillEffect();

                button.onClick.RemoveListener(SkillActivate);
            }
            else
            {
                return;
            }
        }

        private void skillEffect()
        {
            if (skillobject3.skillPointsInvested > 0)
            {
                knowledgeBoost = true;
            }
        }


        void Update()
        {                           
            cost3.text = "One Time Cost: " + skillobject3.expCost.ToString();
            
        }
    }
}
