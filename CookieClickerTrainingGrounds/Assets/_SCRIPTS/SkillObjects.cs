﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace LAPMTech
{
    [CreateAssetMenu(fileName = "Skill", menuName = "ScriptableObjects/Create New Skill", order = 1)]
    public class SkillObjects : ScriptableObject
    {
        public string skillName;

        public int skillID;

        public float expPerSkillPoint;

        public float expCost;

        public float costIncrease;

        public float expSum;

        public float skillPointsInvested;

        public bool oneTimeUse;

        public bool knowledgeBoost;

        public bool silverBoost;

        public bool coordination;


    }
}