﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LAPMTech
{ 

    public class Clicker : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI ExpCount;

        [SerializeField]
        private Button button;

        [SerializeField]
        private Skill_06 coop;

        private bool somethingWasBought;

        public bool SomethingWasBought
        {
            get
            {
                return somethingWasBought;
            }
            set
            {
                somethingWasBought = value;
            }
        }

        private float experiencePoints = 0;

        public float ExperiencePoints
        {
            get
            {
                return experiencePoints;
            }
            set
            {
                experiencePoints = value;
            }
        }

        void Start()
        {           
            ExpCount = GetComponent<TextMeshProUGUI>();
            ExpCount.text = ("Experience: " + ExperiencePoints);
            button = GetComponentInParent<Button>();        
            button.onClick.AddListener(GetExperience);
        }

        void GetExperience()
        {
            experiencePoints++;         
            Debug.Log("You have " + ExperiencePoints + " experience point(s)");

            if (coop.CoordinatedAtt == true)
            {
                experiencePoints += coop.CoordinatedSum;
            }
            
        }
        
        void Update()
        {
            ExpCount.text = ("Experience: " + ExperiencePoints);
        }
    }
}