﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
    public class Skill_06 : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI cost6;

        [SerializeField]
        private SkillObjects skillobject6;

        [SerializeField]
        private Clicker clicker;

        [SerializeField]
        private Button button;

        [SerializeField]
        private Skill_01 skillSum1;

        [SerializeField]
        private Skill_02 skillSum2;

        private bool coordinatedAtt;

        private float coordinatedSum;

        public float CoordinatedSum
        {
            get
            {
                return coordinatedSum;
            }
            set
            {

                coordinatedSum = value;
            }
        }

        public bool CoordinatedAtt
        {
            get
            {
                return coordinatedAtt;
            }
            set
            {
                coordinatedAtt = value;
            }
        }

        private float skill6AppliedEffect;


        // Start is called before the first frame update

        void Start()
        {
            button.onClick.AddListener(SkillActivate);
            skillobject6.expPerSkillPoint = 1.5f;
            skillobject6.skillPointsInvested = 0;
            skillobject6.expCost = 10000;
        }

        void SkillActivate()
        {
            if (clicker.ExperiencePoints >= skillobject6.expCost)
            {
                clicker.ExperiencePoints -= skillobject6.expCost;

                skillobject6.skillPointsInvested += 1;

                button.interactable = false;

                skillEffect();

                coordinatedAtt = true;

                clicker.SomethingWasBought = true;

                button.onClick.RemoveListener(SkillActivate);
            }
            else
            {
                return;
            }
        }

        private void skillEffect()
        {
            if (skillobject6.skillPointsInvested > 0 && coordinatedAtt == true)
            {
                coordinatedSum = (skillSum1.Sum1 + skillSum2.Sum2) * 1.5f;              
            }
        }


        void Update()
        {          

            cost6.text = "One Time Cost: " + skillobject6.expCost.ToString();

        }
    }
}