using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LAPMTech
{
    public class SkillTree : MonoBehaviour
    {
        [SerializeField]
        private Clicker clicker;
        /// <summary>
        /// List of the buttons we place in the Scene
        /// </summary>
        [SerializeField]
        private List<Button> buttonSkills;

        /// <summary>
        /// Private List of SkillStatus, using internally
        /// </summary>
        private List<SkillStatusBehaviour> skillStatusBehaviours = new List<SkillStatusBehaviour>();

        private void Start()
        {
            // Going through all buttons/skills in a for loop
            for (int i = 0; i < buttonSkills.Count; i++)
            {
                //Populating the skillStatus list, Only works this way when SkillStatusBehaviour is attached to the button GameObjects
                skillStatusBehaviours.Add(buttonSkills[i].GetComponent<SkillStatusBehaviour>());

                // creating a local variable to get value of index
                // This is important in order to "solve" the closure problem. For reference google for "C# closures"
                int closureIndex = i;

                // Registering the onClick Event of the buttons to our SetSkillActive Method
                buttonSkills[closureIndex].onClick.AddListener(delegate { SetNextSkillActive(skillStatusBehaviours[closureIndex].ButtonId); });

                // If the .IsAvailable Property is set to false we also set the .interactable field of the respective button to false
                if (!skillStatusBehaviours[i].IsAvailable)
                {
                    buttonSkills[i].interactable = false;
                }
            }
        }

        /// <summary>
        /// Activates the next skill in our skilltree
        /// </summary>
        /// <param name="buttonId"></param>
        private void SetNextSkillActive(int buttonId)
        {
            if (clicker.SomethingWasBought == true)
            {
                int next = buttonId + 1;
                if (next >= skillStatusBehaviours.Count)
                {
                    return;
                }

                skillStatusBehaviours[next].IsAvailable = true;
                buttonSkills[next].interactable = skillStatusBehaviours[next].IsAvailable;
                clicker.SomethingWasBought = false;
            }
        }
    }
}