﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LAPMTech
{

    public class UpgradeWindowButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        private bool WindowEnabler;

        [SerializeField]
        private Transform WindowExitDestination;

        [SerializeField]
        private Transform WindowOpenDestination;

        private Vector3 WindowPosition;

        [SerializeField]
        private float movementSpeed;

        void Start()
        {
            WindowPosition = GetComponent<RectTransform>().position;           
            button.onClick.AddListener(UpgradeWindowEnabler);
        }

        void UpgradeWindowEnabler()
        {
            WindowEnabler = true;
        }

        void UpgradeWindow()
        {
            if (WindowEnabler == true)
            {
                Vector3 WindowPosition = Vector3.MoveTowards(transform.position, WindowOpenDestination.position, movementSpeed * Time.deltaTime);
                transform.position = WindowPosition;

                if (Vector3.Distance(transform.position, WindowOpenDestination.position) < 0.1f)
                {                  
                    WindowEnabler = false;
                }
            }
        }

        void Update()
        {
            UpgradeWindow();
        }
    }
}