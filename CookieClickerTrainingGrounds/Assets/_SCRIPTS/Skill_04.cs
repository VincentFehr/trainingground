﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
    public class Skill_04 : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI cost4;

        [SerializeField]
        private SkillObjects skillobject4;

        [SerializeField]
        private Clicker clicker;

        [SerializeField]
        private Button button;

        private bool silverEnhancement;

        public bool SilverEnhancement
        {
            get
            {
                return silverEnhancement;
            }
            set
            {
                silverEnhancement = value;
            }
        }

        private float skill4AppliedEffect;



        // Start is called before the first frame update

        void Start()
        {
            button.onClick.AddListener(SkillActivate);
            skillobject4.expPerSkillPoint = 10;
            skillobject4.skillPointsInvested = 0;
            skillobject4.expCost = 5000;
        }

        void SkillActivate()
        {
            if (clicker.ExperiencePoints >= skillobject4.expCost)
            {
                clicker.ExperiencePoints -= skillobject4.expCost;

                skillobject4.skillPointsInvested += 1;

                button.interactable = false;

                clicker.SomethingWasBought = true;
            }
            else
            {
                return;
            }
        }

        private void skillEffect()
        {
            if (skillobject4.skillPointsInvested > 0)
            {
                silverEnhancement = true;
            }
        }


        void Update()
        {
            skill4AppliedEffect = skillobject4.skillPointsInvested * skillobject4.expPerSkillPoint;
        
            cost4.text = "One Time Cost: " + skillobject4.expCost.ToString();

        }
    }
}
