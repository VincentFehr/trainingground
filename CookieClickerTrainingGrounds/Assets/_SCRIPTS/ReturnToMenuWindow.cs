﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    public class ReturnToMenuWindow : MonoBehaviour
    {
        [SerializeField]
        private GameObject returnToMenuWindow;

        // Start is called before the first frame update
        void Start()
        {

        }

        public void returnWindow()
        {
            returnToMenuWindow.gameObject.SetActive(true);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
