﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace LAPMTech
{
    public class Skill_01 : MonoBehaviour
    {
        [SerializeField]
        private SkillObjects skillobject1;

        [SerializeField]
        private TextMeshProUGUI cost1;

        [SerializeField]
        private TextMeshProUGUI numberOfUnits1;

        [SerializeField]
        private Skill_03 knowledge;

        [SerializeField]
        private Skill_05 squireBuff;

        [SerializeField]
        private Clicker clicker;

        [SerializeField]
        private Button button;

        private bool tooStrong;

        private float sum1;

        public float Sum1
        {
            get
            {
                return sum1;
            }
            set
            {
                sum1 = value;
            }
        }

        private float skill1AppliedEffect;    

        private float nextActionTime = 0.0f;

        private float period = 1f;
     
        // Start is called before the first frame update

        void Start()
        {                    
            button.onClick.AddListener(SkillActivate);
            skillobject1.expPerSkillPoint = 2;
            skillobject1.skillPointsInvested = 0;
            skillobject1.knowledgeBoost = false;
            skillobject1.expCost = 5;
        }

        void SkillActivate()
        {
            if(clicker.ExperiencePoints >= skillobject1.expCost)
            {
                clicker.ExperiencePoints -= skillobject1.expCost;

                skillobject1.skillPointsInvested += 1;

                clicker.SomethingWasBought = true;
            }
            else
            {
                return;
            }
        }

        private void skillEffect()
        {
            if(skillobject1.skillPointsInvested > 0 && skillobject1.knowledgeBoost == false)
            {
                skillobject1.expSum = skill1AppliedEffect;
                clicker.ExperiencePoints += skillobject1.expSum;
            }
            else if (skillobject1.skillPointsInvested > 0 && skillobject1.knowledgeBoost == true)
            {
                skillobject1.expSum = skill1AppliedEffect * 1.25f;
                clicker.ExperiencePoints += skillobject1.expSum;
            }
            
        }

        void buffTime()
        {
            if (squireBuff.SquireStrong == true && tooStrong == false)
            {
                skillobject1.expPerSkillPoint += 20;
                tooStrong = true;
            }
        }

        void Update()
        {
            buffTime();
            skill1AppliedEffect = skillobject1.skillPointsInvested * skillobject1.expPerSkillPoint;
            if (skillobject1.skillPointsInvested > 0)
            {
                skillobject1.expCost = skillobject1.skillPointsInvested * skillobject1.costIncrease;
            }
            if (Time.time > nextActionTime)
            {
                nextActionTime += period;

                skillEffect();

            }
            skillobject1.knowledgeBoost = knowledge.KnowledgeBoost;            
            
            skillobject1.expSum = Sum1;
            cost1.text = "Cost: " + skillobject1.expCost.ToString();
            numberOfUnits1.text = "Units: " + skillobject1.skillPointsInvested.ToString();

        }

    }
}
