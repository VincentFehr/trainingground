﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
    public class Skill_05 : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI cost5;

        [SerializeField]
        private SkillObjects skillobject5;

        [SerializeField]
        private TextMeshProUGUI numberOfUnits5;

        [SerializeField]
        private Clicker clicker;

        [SerializeField]
        private Button button;

        private bool squireStrong;

        public bool SquireStrong
        {
            get
            {
                return squireStrong;
            }
            set
            {
                squireStrong = value;
            }          
        }

        private float skill5AppliedEffect;


        // Start is called before the first frame update

        void Start()
        {
            button.onClick.AddListener(SkillActivate);
            skillobject5.expPerSkillPoint = 1;
            skillobject5.skillPointsInvested = 0;
            skillobject5.expCost = 6500;
        }

        void SkillActivate()
        {
            if (clicker.ExperiencePoints >= skillobject5.expCost)
            {
                clicker.ExperiencePoints -= skillobject5.expCost;

                skillobject5.skillPointsInvested += 1;

                skillEffect();

                clicker.SomethingWasBought = true;
            }
            else
            {
                return;
            }
        }

        private void skillEffect()
        {
            if (skillobject5.skillPointsInvested > 0)
            {
                squireStrong = true;
            }
        }


        void Update()
        {
            skill5AppliedEffect = skillobject5.skillPointsInvested * skillobject5.expPerSkillPoint;
            if (skillobject5.skillPointsInvested > 0)
            {
                skillobject5.expCost = skillobject5.skillPointsInvested * skillobject5.costIncrease;
            }
            cost5.text = "Cost: " + skillobject5.expCost.ToString();
            numberOfUnits5.text = "Units: " + skillobject5.skillPointsInvested.ToString();
        }
    }
}
