﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace LAPMTech
{
    public class Skill_02 : MonoBehaviour
    {      
        [SerializeField]
        private TextMeshProUGUI cost2;

        [SerializeField]
        private TextMeshProUGUI numberOfUnits2;

        [SerializeField]
        private SkillObjects skillobject2;

        [SerializeField]
        private Skill_03 knowledge;

        [SerializeField]
        private Clicker clicker;

        [SerializeField]
        private Button button;

        [SerializeField]
        private Button clickerButton;

        [SerializeField]
        private Skill_04 silver;

        private bool adeptSlash;

        private float skill2AppliedEffect;

        private float sum2;

        public float Sum2
        {
            get
            {
                return sum2;
            }
            set
            {
                sum2 = value;
            }
        }

        // Start is called before the first frame update

        void Start()
        {
            button.onClick.AddListener(SkillActivate);
            skillobject2.expPerSkillPoint = 5;
            skillobject2.skillPointsInvested = 0;
            skillobject2.knowledgeBoost = false;
            clickerButton.onClick.AddListener(skillEffect);
            skillobject2.expCost = 75;
        }

        void SkillActivate()
        {
            if (clicker.ExperiencePoints >= skillobject2.expCost)
            {
                clicker.ExperiencePoints -= skillobject2.expCost;

                skillobject2.skillPointsInvested += 1;

                clicker.SomethingWasBought = true;

            }
            else
            {
                return;
            }
        }

        private void skillEffect()
        {
           
      
            if (skillobject2.skillPointsInvested > 0 && skillobject2.knowledgeBoost == true)
            {
                skillobject2.expSum = skill2AppliedEffect * 1.25f;
                clicker.ExperiencePoints += skillobject2.expSum;
                
            }
            else if (skillobject2.skillPointsInvested > 0 && skillobject2.knowledgeBoost == false)
            {
                    skillobject2.expSum = skill2AppliedEffect;
                    clicker.ExperiencePoints += skillobject2.expSum;

            }

            
        }


        void Update()
        {

            if (skillobject2.silverBoost == true)
            {
                skill2AppliedEffect = (skillobject2.skillPointsInvested * skillobject2.expPerSkillPoint) + (10 * skillobject2.skillPointsInvested);
            }
            else
            {
                skill2AppliedEffect = skillobject2.skillPointsInvested * skillobject2.expPerSkillPoint;
            }
            if(skillobject2.skillPointsInvested > 0)
            { 
                skillobject2.expCost = skillobject2.skillPointsInvested * skillobject2.costIncrease;
            }
            skillobject2.knowledgeBoost = knowledge.KnowledgeBoost;

            skillobject2.silverBoost = silver.SilverEnhancement;

            cost2.text = "Cost: " + skillobject2.expCost.ToString();
            numberOfUnits2.text = "Units: " + skillobject2.skillPointsInvested.ToString();
            skillobject2.expSum = Sum2;
        }

    }
}