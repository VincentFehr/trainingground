﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LAPMTech
{
    public class Tooltip : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI tooltipText;

        [SerializeField]
        private Skill_01 skill_01;
        [SerializeField]
        private Skill_01 skill_02;
        [SerializeField]
        private Skill_01 skill_03;
        [SerializeField]
        private Skill_01 skill_04;
        [SerializeField]
        private Skill_01 skill_05;
        [SerializeField]
        private Skill_01 skill_06;
    
        private string Skill1 = "Skill1";

        private string Skill2 = "Skill2";

        private string Skill3 = "Skill3";

        private string Skill4 = "Skill4";

        private string Skill5 = "Skill5";

        private string Skill6 = "Skill6";

        public void InfoText()
        {
            if (gameObject.tag == Skill1)
            {
                tooltipText.text = "Squire:" +
                    " Gain 2 experience per second for every skill point invested.";
            }
            else if (gameObject.tag == Skill2)
            {
                tooltipText.text = "Adept Hunter:" +
                    " For every skill point gain 5 more experience per slash.";
            }
            else if (gameObject.tag == Skill3)
            {
                tooltipText.text = "Knowledge is Power:" +
                    " Increase your experience gain by 25%.";
            }
            else if (gameObject.tag == Skill4)
            {
                tooltipText.text = "Silver Enhancement:" +
                    " Increase your adept hunters experience gain by 10 per slash.";
            }
            else if (gameObject.tag == Skill5)
            {
                tooltipText.text = "Masterful Swordsman:" +
                    " Increase your squires experience gain per second by 20 points per unit.";
            }
            else if (gameObject.tag == Skill6)
            {
                tooltipText.text = "Coordinated Attack:" +
                    " The squires and the adept hunters gain 50% more experience of their combined gain per slash.";
            }

        }


        // Start is called before the first frame update
        void Start()
        {
            tooltipText.text = "For more information, hover over a skill!";
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
