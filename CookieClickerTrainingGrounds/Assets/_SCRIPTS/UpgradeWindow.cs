﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LAPMTech
{
    public class UpgradeWindow : MonoBehaviour
    {
        [SerializeField]
        private Button enterButton;


        [SerializeField]
        private Button exitButton;

        private bool WindowEnabler;

        private bool WindowDisabler;

        [SerializeField]
        private Transform WindowExitDestination;

        [SerializeField]
        private Transform WindowOpenDestination;

        private Vector3 WindowEnterPosition;

        private Vector3 WindowExitPosition;

        [SerializeField]
        private float movementSpeed;

        void Start()
        {
            enterButton.onClick.AddListener(UpgradeWindowEnabler);
            exitButton.onClick.AddListener(UpgradeWindowDisabler);

        }

        void UpgradeWindowEnabler()
        {
            WindowEnabler = true;
        }

        void UpgradeWindowEnter()
        {
            if (WindowEnabler == true)
            {
                enterButton.interactable = false;
                exitButton.interactable = false;

                Vector3 WindowEnterPosition = Vector3.MoveTowards(transform.position, WindowOpenDestination.position, movementSpeed * Time.deltaTime);
                transform.position = WindowEnterPosition;

                if (Vector3.Distance(transform.position, WindowOpenDestination.position) < 0.1f)
                {
                    WindowEnabler = false;
                    exitButton.interactable = true;
                    enterButton.interactable = true;
                }
            }
        }

        void UpgradeWindowDisabler()
        {
            WindowDisabler = true;
        }

        void UpgradeWindowExit()
        {
            if (WindowDisabler == true)
            {
                enterButton.interactable = false;
                exitButton.interactable = false;

                Vector3 WindowExitPosition = Vector3.MoveTowards(transform.position, WindowExitDestination.position, movementSpeed * Time.deltaTime);
                transform.position = WindowExitPosition;

                if (Vector3.Distance(transform.position, WindowExitDestination.position) < 0.1f)
                {
                    WindowDisabler = false;
                    exitButton.interactable = true;
                    enterButton.interactable = true;
                }
            }
        }

        void Update()
        {
            UpgradeWindowEnter();
            UpgradeWindowExit();
        }
    }
}

