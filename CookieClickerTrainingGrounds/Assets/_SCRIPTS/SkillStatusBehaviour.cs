﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace LAPMTech
{
    public class SkillStatusBehaviour : MonoBehaviour
    {
        [SerializeField]
        private bool isActive;

        [SerializeField]
        private bool isAvailable;

        [SerializeField]
        private int buttonId;

        private Button button;

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public bool IsAvailable
        {
            get { return isAvailable; }
            set { isAvailable = value; }
        }

        public int ButtonId
        {
            get { return buttonId; }
            set { buttonId = value; }
        }
    }
}